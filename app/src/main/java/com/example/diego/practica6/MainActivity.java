package com.example.diego.practica6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    Button save, load;
    EditText txtContacto,txtEmail,txtTelefono;
    String Contacto,Email,Telefono;
    String resContacto="";
    String resEmail ="";
    String resTelefono="";
    int data_block =100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        save = (Button) findViewById(R.id.SAVE);
        load =(Button) findViewById(R.id.LOAD);

        txtContacto =(EditText) findViewById(R.id.CONTACTO);
        txtEmail =(EditText) findViewById(R.id.EMAIL);
        txtTelefono =(EditText) findViewById(R.id.TELEFONO);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contacto = txtContacto.getText().toString();
                Email = txtEmail.getText().toString();
                Telefono = txtTelefono.getText().toString();

                try {
                    FileOutputStream fou = openFileOutput("contactos.txt", MODE_WORLD_READABLE);
                    OutputStreamWriter osw = new OutputStreamWriter(fou);

                    try {
                        osw.write(Contacto);
                        osw.write(";");
                        osw.write(Email);
                        osw.write(";");
                        osw.write(Telefono);
                        osw.write(";");
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(), "Contacto Guardado", Toast.LENGTH_LONG).show();

                    } catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view)
            {
                try {
                    FileInputStream fis = openFileInput("contactos.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    char[] data = new char[data_block];
                    String final_data = "";
                    int size;

                    try {
                        while ((size = isr.read(data)) > 0) {

                            String read_data = String.valueOf(data, 0, size);
                            final_data += read_data;
                            data = new char[data_block];
                        }
                        resContacto=final_data.split(";")[0];
                        resEmail=final_data.split(";")[1];
                        resTelefono=final_data.split(";")[2];
                        Toast.makeText(getBaseContext(), "Contacto: " + resContacto +"\n"+ "Email: " + resEmail +"\n"+ "Teléfono: " + resTelefono, Toast.LENGTH_LONG).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }


        });


    }
}
